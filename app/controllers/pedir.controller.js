const dddModel = require("../models/ddd.model");
const horasModel = require("../models/horas.model");
const pagamentoModel = require("../models/pagamento.model");
const optionsMapper = require("../utils/mappers/select-options.mapper");

const fs = require("fs");
const Joi = require("joi");
const ejs = require("ejs");
const htmlToPdf = require("html-pdf-node");
const path = require("path");

// const modelSexo = require("../models/sexo.model");

const getPedir = (req, res, next) => {
  //todo: construir viewmodel
  const viewModel = {
     opcoesDDD: optionsMapper("descricao", "id", dddModel.listaTodos()),
     opcoesHoras: optionsMapper("descricao", "id", horasModel.listaTodos()),
     opcoesPagamento: optionsMapper("descricao", "id", pagamentoModel.listaTodos())     
  };

  //todo:  construir view
  res.render("pedir", viewModel);
};

function convertDate(dateString) {
  var p = dateString.split(/\D/g)
  return [p[2], p[1], p[0]].join("/")
}

const postPedir = (req, res, next) => {

  //TODO: CRIAR VALIDACOES DE NEGOCIO




  //TODO: montar o viewmodel

  const { nome, sobrenome, telefone, ddd, cep, rua, numero, complemento, bairro, referencia, talheres, observacoes, totalgeral, listageral, horas, minutos, data, entrega, pagamento, troco, troco_para, troco_separar } = req.body;
  
  const dddSelecionado = dddModel.BuscaPorId(ddd);
  const pagamentoSelecionado = pagamentoModel.BuscaPorId(pagamento);
  
  let horasPesq = ""

  if (horas == "") {
    horasPesq = "1"
  } else {
    horasPesq = horas
  }

  const horasSelecionado = horasModel.BuscaPorId(horasPesq);
  const listageraljson = JSON.parse(listageral)
  const databrasil = convertDate(data)
  let troco_cliente = (troco_para - troco_separar)
  troco_cliente = parseFloat(troco_cliente).toFixed(2)

  const pdfViewModel = {
    nome,
    sobrenome,
    ddd: dddSelecionado.descricao,
    telefone,
    cep,
    rua,
    numero,
    complemento,
    bairro,
    referencia,
    talheres,
    observacoes,
    totalgeral,
    listageraljson,
    horas: horasSelecionado.descricao,
    minutos,
    databrasil,
    entrega,
    pagamento: pagamentoSelecionado.descricao,
    troco,
    troco_para,
    troco_cliente

    // estadocivil: estadoCivilSelecionado.descricao,
    // profissao: profissaoSelecionado.nome,
  };

  //TODO: montar o html

  const filePath = path.join(__dirname, "../views/pedir-pdf.ejs");

  console.log(filePath);

  const templateHtml = fs.readFileSync(filePath, 'utf8');

  //TODO: montar o pdf
  const htmlPronto = ejs.render(templateHtml, pdfViewModel);

  //TODO: retornar o pdf

  const file = {
    content: htmlPronto
  };

  const configuracoes = {
    format: 'A4',
    printBackground: true
  };

  htmlToPdf.generatePdf(file, configuracoes)
    .then((resultPromessa) => {
      res.contentType("application/pdf");
      res.send(resultPromessa);
    });

};

const postPedirSchema = Joi.object({
  nome: Joi.string().required(),
  sobrenome: Joi.string().required(),
  cpf: Joi.number().required(),
  cep: Joi.number().required(),
  telefone: Joi.number().required(),
  // }).unknown(true);
}).unknown(true);

const getPedirSchema = Joi.object({
  teste: Joi.number().required(),
});


module.exports = {
  getPedir,
  postPedir,
  postPedirSchema,
  getPedirSchema
};
