const sexoModel = require("../models/sexo.model");
const estadoCivilModel = require("../models/estado-civil.model");
const profissaoModel = require("../models/profissao.model");
const optionsMapper = require("../utils/mappers/select-options.mapper");

const fs = require("fs");
const Joi = require("joi");
const ejs = require("ejs");
const htmlToPdf = require("html-pdf-node");
const path = require("path");

// const modelSexo = require("../models/sexo.model");

const getPedir = (req, res, next) => {
  //todo: construir viewmodel
  const viewModel = {
    nome: "",
    title: "Liniker é o Professor",
    opcoesSexo: optionsMapper("descricao", "id", sexoModel.listaTodos()),
    opcoesEstadoCivil: optionsMapper(
      "descricao",
      "id",
      estadoCivilModel.listaTodos()
    ),
    opcoesProfissao: optionsMapper("nome", "id", profissaoModel.listaTodos()),
    comboEstado: (e) => console.log(e),
  };

  //todo:  construir view
  res.render("pedir", viewModel);
};

const postPedir = (req, res, next) => {
  
  //TODO: CRIAR VALIDACOES DE NEGOCIO
  
  
  
  
  //TODO: montar o viewmodel

  const { nome, email, datanascimento, sexo, estadocivil, profissao } = req.body;
  const sexoSelecionado = sexoModel.BuscaPorId(sexo);
  const estadoCivilSelecionado = estadoCivilModel.BuscaPorId(estadocivil);
  const profissaoSelecionado = profissaoModel.BuscaPorId(profissao);

  const pdfViewModel = {
    nome, 
    datanascimento,
    email,
    sexo: sexoSelecionado.descricao,
    estadocivil: estadoCivilSelecionado.descricao,
    profissao: profissaoSelecionado.nome,
  };
    
  //TODO: montar o html

  const filePath = path.join(__dirname, "../views/pedir-pdf.ejs");

  console.log(filePath);

  const templateHtml = fs.readFileSync(filePath, 'utf8');
  
  //TODO: montar o pdf
  const htmlPronto = ejs.render(templateHtml, pdfViewModel);

  //TODO: retornar o pdf

  const file = {
    content: htmlPronto  
  };

  const configuracoes = {
    format: 'A4',
    printBackground: true
  };

  htmlToPdf.generatePdf(file, configuracoes)
  .then((resultPromessa) => {
      res.contentType("application/pdf");
      res.send(resultPromessa);
    });

};

const postPedirSchema = Joi.object({
  nome: Joi.string().max(30).min(5).required().custom((value, helpers) => {
    const result = value.split(' ');
    if (result.length > 1) {
      return value;
    }
    return helpers.error("any.invalid");
  }),

  email:  Joi.string().email().required(),
  datanascimento: Joi.date().iso().required(),
  sexo: Joi.number().required(),
  estadocivil: Joi.number().required(),
  profissao: Joi.number().required(),
// }).unknown(true);
});

const getPedirSchema = Joi.object({
  teste: Joi.number().required(),
});


module.exports = {
  getPedir,
  postPedir,
  postPedirSchema,
  getPedirSchema
};
