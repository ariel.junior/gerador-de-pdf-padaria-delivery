
const express = require("express");
const v1Inicio = require('./v1/inicio');
// const sobre = require("./v1/sobre");
const v1Sobre = require('./v1/sobre');
const v1Pedir = require('./v1/pedir');
const v1Contato = require('./v1/contato');


module.exports = (app) => {
    const routev1 = express.Router();
    
    v1Inicio(routev1);
    v1Sobre(routev1);
    v1Pedir(routev1);
    v1Contato(routev1);

    app.use('', routev1); // foi o liniker que tirou
}

