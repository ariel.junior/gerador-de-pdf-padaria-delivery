
const contatoController = require("../../controllers/contato.controller");

module.exports = (routeV1) => {
    routeV1.route('/contato')
        .get(contatoController.getContato);
}