
const inicioController = require("../../controllers/inicio.controller");

module.exports = (routeV1) => {

    routeV1.route('/')
        .get(
            (req, res, next) => {
                console.log("meu middleware");
                next()
            },
            inicioController.getInicio
        );
        
}