const TabelaDB = [
{id: "prod1", valor: "valor1", preco: 0.5, produto: "Pão Francês"},
{id: "prod2", valor: "valor2", preco: 8.5, produto: "Pão de Forma"},
{id: "prod3", valor: "valor3", preco: 4.9, produto: "Queijo Mussarela"},
{id: "prod4", valor: "valor4", preco: 5.2, produto: "Queijo Prato"},
{id: "prod5", valor: "valor5", preco: 7.2, produto: "Requeijão"},
{id: "prod6", valor: "valor6", preco: 3.5, produto: "Manteiga barra"},
{id: "prod7", valor: "valor7", preco: 4.9, produto: "Pão de Queijo"},
{id: "prod8", valor: "valor8", preco: 3.7, produto: "Leite integral"},
{id: "prod9", valor: "valor9", preco: 3.3, produto: "Leite desnatado"},
{id: "prod10", valor: "valor10", preco: 7.0, produto: "Coca-Cola"}
]


function verifica() {

  let total = 0;
  let talher = 0;

  for (let i = 1; i < 11; i++) {
    let valor = document.getElementById("prod" + i).value;

    const preco = TabelaDB.find(item => item.id == ("prod" + i))
    
    let valortxt = ""
    if (valor == 0) {
      valortxt = "R$ 0.00"
    } else {
      valortxt = "R$ " + (parseFloat((valor * preco.preco)).toFixed(2))
    }
        document.getElementById("valor" + i).textContent = valortxt
   total = total + (valor * preco.preco)
  }

  if (document.getElementById("talheres").checked == true) {
    talher = 1;
    total = total + talher
  }

  document.getElementById("total").textContent = "R$ " + parseFloat(total).toFixed(2)

}